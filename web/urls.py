from django.conf.urls import url
from django.views.generic import TemplateView

urlpatterns = (
    url(r'^$', TemplateView.as_view(template_name='home.html'), name='home'),
    url(r'^servicios/$', TemplateView.as_view(template_name='servicios.html'), name='services'),
    url(r'^contacto/$', TemplateView.as_view(template_name='contacto.html'), name='contact'),
)